/*
1. Якщо говорити про будь-який ввід у поле <input>, подій клавіатури недостатньо. Події клавіатури слід використовувати, коли необхідно обробляти конкреино лише дії клавіатури. 
*/

let highlightBtn = () => {
let btn = document.getElementsByTagName('BUTTON');

document.addEventListener('keydown', (event) => {
  for (let item of btn) {
  item.style.backgroundColor = 'black';
  if (item.innerText.toUpperCase() == event.key.toUpperCase()) {
  item.style.backgroundColor = 'blue';
  }}})};
highlightBtn();